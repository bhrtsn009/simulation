#!/usr/bin/env escript
% Sets all pps stations in debug mode

main(_) ->
    net_kernel:start([shell, shortnames]),
    erlang:set_cookie(node(), butler_server),
    Result=(rpc:call(erlang:list_to_atom("butler_server@localhost"),erlang, exit, [rpc:call(erlang:list_to_atom("butler_server@localhost"),global, whereis_name, [butler_manager_sup]),kill])),
    io:format("Result: ~p~n",[Result]).
