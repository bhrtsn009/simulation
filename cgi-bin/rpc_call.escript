#!/usr/bin/env escript
% Sets all pps stations in debug mode

main([M, F, A]) ->
    {ok,Tokens,_EndLine} = erl_scan:string(A),
    {ok,AbsForm} = erl_parse:parse_exprs(Tokens),
    {value,Value,_Bs} = erl_eval:exprs(AbsForm, erl_eval:new_bindings()),
    net_kernel:start([shell, shortnames]),
    erlang:set_cookie(node(), butler_server),
    R = rpc:call(erlang:list_to_atom("butler_server@localhost"), list_to_atom(M), list_to_atom(F), Value),
    io:format("Result: ~p~n",[R]),
    R.
